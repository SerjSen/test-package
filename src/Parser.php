<?php

namespace mr1hide\parser;

class Parser implements ParserInterface
{

    public function process(string $url,string $tag): array
    {
        $pattern = "/<$tag.*?>(.+?)<\/$tag>/su";
        $resource = file_get_contents($url);
        preg_match_all($pattern, $resource, $matches, PREG_SET_ORDER, 0);

        return $matches;
    }
}
